﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace VectorCA {
    public class Board<T> {
        public class XYPair {
            public int X;
            public int Y;

            public XYPair(int x, int y) {
                X = x;
                Y = y;
            }

            public override bool Equals(object obj) {
                var pair = obj as XYPair;
                if (pair == null) { return false; }
                return pair.X == X && pair.Y == Y;
            }

            public override int GetHashCode() {
                return X.GetHashCode() ^ Y.GetHashCode();
            }

            public static implicit operator XYPair((int, int) tuple) {
                return new XYPair(tuple.Item1, tuple.Item2);
            }

            public static implicit operator (int, int) (XYPair pair) {
                return (pair.X, pair.Y);
            }

            public void Deconstruct(out int x, out int y) {
                x = X;
                y = Y;
            }
        }

        Cell<T>[,] cells = new Cell<T>[2000, 2000];
        public HashSet<XYPair> aliveCellIndex = new HashSet<XYPair>();

        public Cell<T> this[int x, int y] => cells[x, y];

        static Func<int, int, int, int, float> distance = (x1, y1, x2, y2) => (float)Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        static Func<int, int, int, int, float, float> scaleFunc = (x1, y1, x2, y2, v) => .00000008f * v / (float)Math.Pow(distance(x1, y1, x2, y2), 2);

        public void PlaceMass(int x, int y, int mass, T obj) {
            cells[x, y].Mass = mass;
            cells[x, y].Object = obj;
            aliveCellIndex.Add((x, y));
            calculateVectors();
        }

        public bool IsInBounds(int x, int y) => x >= 0 && x < 2000 && y >= 0 && y <= 2000;

        public void Step() {
            Cell<T>[,] newCells = new Cell<T>[2000, 2000];
            HashSet<XYPair> newCellIndex = new HashSet<XYPair>();

            foreach ((var x, var y) in aliveCellIndex) {
                var cell = cells[x, y];
                int newX = x + (int)cell.Vector.X;
                int newY = y + (int)cell.Vector.Y;

                newCells[newX, newY].Mass = cell.Mass;
                newCells[newX, newY].Object = cell.Object;
                newCellIndex.Add((newX, newY));

            }

            cells = newCells;
            aliveCellIndex = newCellIndex;
            calculateVectors();
        }

        private void calculateVectors() {
            foreach ((var x, var y) in aliveCellIndex) {
                var aliveCell = cells[x, y];
                for (int i = -200; i <= 200; i++) {
                    for (int j = -200; j <= 200; j++) {
                        var newX = x + i;
                        var newY = y + j;
                        if (distance(x, y, newX, newY) > 200) {
                            continue;
                        }
                        var c = cells[newX, newY];
                        var scale = scaleFunc(x, y, newX, newY, aliveCell.Mass * (c.Mass == 0 ? 1 : c.Mass));
                        if (!float.IsInfinity(scale)) {
                            cells[newX, newY].Vector += scale * -new Vector2(i, j);
                        }
                    }
                }
            }
        }
    }
}
