﻿using System;
using System.Numerics;

namespace VectorCA {
    public struct Cell<T> {
        public Vector2 Vector { get; set; }
        public int Mass { get; set; }
        public T Object { get; set; }
    }
}
