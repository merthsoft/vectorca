﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using VectorCA;
using MonoGame.Extended;
using System;

namespace VectorCAGame {
    public class Game1 : Game {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        Board<Color> board;
        private MouseState previousMouseState;
        private MouseState currentMouseState;
        Size2 drawSize = new Size2(1, 1);
        Vector2 window = new Vector2(800, 800);
        Size2 windowSize = new Size2(400, 400);

        KeyboardState previousKeyboardState;
        KeyboardState currentKeyboardState;

        public Game1() {
            graphics = new GraphicsDeviceManager(this);

            graphics.PreferredBackBufferHeight = 400;
            graphics.PreferredBackBufferWidth = 400;

            Content.RootDirectory = "Content";
            IsMouseVisible = true;
        }

        protected override void Initialize() {
            board = new Board<Color>();

            board.PlaceMass(1000, 1000, 166053904, Color.Pink);
            board.PlaceMass(1050, 1050, 166053904, Color.Plum);
            board.PlaceMass(1020, 980, 166053904, Color.Plum);
            var vec = board[999, 999].Vector;
            //Console.WriteLine(vec);

            base.Initialize();
        }

        protected override void LoadContent() {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
        }

        protected override void Update(GameTime gameTime) {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            previousMouseState = currentMouseState;
            currentMouseState = Mouse.GetState();

            if (previousMouseState.X != currentMouseState.X || previousMouseState.Y != currentMouseState.Y) {
                (var x, var y) = ((int)(currentMouseState.X / drawSize.Width + window.X), (int)(currentMouseState.Y / drawSize.Height + window.Y));

                if (board.IsInBounds(x, y)) {
                    Cell<Color> c = board[x, y];
                    Console.WriteLine($"({x}, {y}) = {c.Vector}");
                }
            }

            previousKeyboardState = currentKeyboardState;
            currentKeyboardState = Keyboard.GetState();

            if (currentKeyboardState.IsKeyDown(Keys.Space) && !previousKeyboardState.IsKeyDown(Keys.Space)) {
                board.Step();
                
            }

            base.Update(gameTime);
        }

        //private void saveScreenshot() {
        //    Texture2D rt = new Texture2D(
        //        GraphicsDevice,
        //        graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight,
        //        1,
        //        SurfaceFormat.HalfVector4); // this format works with my GPU.  Yours may be different.
        //                                    // If not working, try a few different ones.

        //    // actually grab the current set of color values from the back buffer
        //    GraphicsDevice.GetBackBufferData<(rt);

        //    // Save out to disk.
        //    rt.Save("screenshot.png", ImageFileFormat.Png); // saves into same dir as .exe
        //}

        protected override void Draw(GameTime gameTime) {
            GraphicsDevice.Clear(Color.CornflowerBlue);
            spriteBatch.Begin();

            for (var i = 0; i < windowSize.Width; i++) {
                for (var j = 0; j < windowSize.Height; j++) {
                    var x = (int)window.X + i;
                    var y = (int)window.Y + j;
                    var cell = board[x, y];

                    var color = Color.Black;
                    if (cell.Mass > 0) {
                        color = cell.Object;
                    } else {
                        //Console.WriteLine(cell.Vector);
                        color = new Color(32*(int)Math.Abs(cell.Vector.X), 32*(int)Math.Abs(cell.Vector.Y), 0);
                    }

                    //spriteBatch.DrawRectangle(new Vector2(i * drawSize.Width, j * drawSize.Height), drawSize, color);
                    spriteBatch.DrawPoint(new Vector2(i, j), color);
                }
            }


            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
